docker-proxay
=============

Container image for [proxay](https://github.com/airtasker/proxay).

> Proxay is a record/replay proxy server that helps you write faster and more reliable tests.


Usage
-----

The image is available as: `registry.gitlab.com/denkmal/docker-proxay:latest`.

Development
-----------

Build the image:
```
podman build -t docker-proxay .
```

Run the image:
```
podman run --rm -p 3000:3000 docker-proxay proxay --help
```

Release
-------

The image is built and pushed to the registry by Gitlab CI.
